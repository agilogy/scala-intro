import java.util.NoSuchElementException

import scala.annotation.tailrec
import scala.util.Try
import scala.util.control.NonFatal
//Introduction


// Everything is an object
1 + 2 * 3 / 23

// Same as
1.+(2.*(3)./(23))

// Functions are objects

def sum(a:Int, b:Int):Int = a + b

def sum34(b:Int):Int = sum(34,b)
// or
val sum34Bis: Int => Int = sum(34,_)

def operate(s:String, f: Int => Int):Int = f(s.toInt)
operate("22", sum34)

/*

And you'll learn all of they names in 5 seconds:

Function0[R] // aka () => R
Function1[P1, R] // aka P1 => R
Function2[P1, P2, R] // aka (P1, P2) => R

No more Consumer, Supplier, Predicate, Function, Operator, BiConsumer, UnaryOperator, BinaryOperator, BiOPredicate, BiFunction...

*/

//Methods are cool

def greet(name:String, greeting:String = "Hello", exclamation:Boolean = false):String =
  s"$greeting $name" + (if(exclamation) "!" else "")

greet("John Snow")
greet("John Snow", "You know nothing")
greet("John Snow", greeting = "You know nothing")
greet("Cersey", exclamation = true)

//Collections

val s = Set(1,2,3)
val l = List(4, 8, 15, 16, 23, 42)
val v = Vector(1,2,3)
val m = Map("i" -> 1, "ii" -> 2, "iii" -> 3, "iv" -> 4)

// By hand...

// Java-style for
def sumJavaStyle(l:List[Int]):Int = {
  var res = 0
  for(i <- l){
    res += i
  }
  res
}

sumJavaStyle(l)

def sumRecOverflow(l:List[Int]):Int = l match {
  case Nil => 0
  case h :: t => h + sumRecOverflow(t)
}

sumRecOverflow(l)

def sumTailrec(l:List[Int]):Int = {
  @tailrec
  def sum0(l:List[Int], acc:Int):Int = l match {
    case Nil => acc
    case h :: t => sum0(t, acc + h)
  }
  sum0(l, 0)
}

//Really?

// map, filter...

// map
l.map(i => sum34(i))
l.map(sum34)
val isOdd: Int => Boolean = i => i % 2 == 1

// foreach
l.foreach(println)

// filter & partition
l.filter(isOdd)
l.partition(isOdd)

// map + flatten = flatMap
def neighbours(i:Int):List[Int] = List(i-1, i, i+1)

l.map(neighbours)
l.map(neighbours).flatten
l.flatMap(neighbours)

// reduce & foldLeft
l.reduce((a,b) => a + b)
//Shorter way
l.reduce(_ + _)
//but remember, scala collections give you a lot of useful methods out of the box ;)
l.sum

//But what if list is empty??
//Kaboom!!
// List.empty[Int].reduce(_ + _)

//foldLeft and foldRight to the rescue!

l.foldLeft(0){
  case (i, acc) => i + acc
}

//Option

//Let's print some hero's name or not...
val john = Option("John")
val aGirl = None

//With pattern matching
john match {
  case Some(n) => println(s"Hello $n")
  case None => println(s"A girl has no name")
}

//With HOF

//Ugly but possible because println side effect call returns Unit and Unit is a Type
john.fold(println("A girl has no name"))(n => println(s"Hello $n"))

//Let's calc the String first!
println(john.fold("A girl has no name")(n => s"Hello $n"))

// Pattern matching

val heroes: List[String] = List("Jon", "Tyrion", "Daenerys")

def containsString(l:List[String], element:String):Boolean = l match {
  case Nil => false
  case h :: tail if h == element => true
  case h :: tail => containsString(tail, element)
}

containsString(heroes, "Arya")

def findString(l:List[String], predicate:String=>Boolean):Option[String] = l match {
  case Nil => None
  case h :: tail if predicate(h) => Some(h)
  case h :: tail => findString(tail, predicate)
}

findString(heroes, _ == "Arya")

//Be generic my friend
def find[T](l:List[T], predicate: T => Boolean): Option[T] = l match {
  case Nil => None
  case h :: tail if predicate(h) => Some(h)
  case h :: tail => find(tail, predicate)
}

find[String](heroes, h => h == "Jon")
find[Int](List(1, 2, 3), (n:Int) => n == 4)

def findOdd: List[Int] => Option[Int] = find[Int](_, i => i % 2 == 1)

findOdd(l)

def findEmptyString: List[String] => Option[String] = find[String](_, s => s.isEmpty)

findEmptyString(heroes)

// Exceptions use pattern matching. Powerful, isn't it?

// By name call
def validate[T](expr: => T): Unit = {
  try {
    expr
  } catch {
    case _:NoSuchElementException => println("gotcha!!")
    case NonFatal(t) => println("What the hell is that???")
  }
}

validate(None.get)
validate(List.empty[Int].reduce(_ + _))

