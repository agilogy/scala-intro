package com.agilogy.tutorial.scala

import com.agilogy.tutorial.scala.model.{Hero, House, Weapon}
import com.agilogy.tutorial.java.model.{Hero => JHero, House => JHouse, Weapon => JWeapon}

object MyHeroApp extends App {
  //Let's play

  val sHero = Hero(
    "Arya",
    House("Stark", "Winter is comming"),
    17,
    Weapon("Needle", 100),
    None
  )

  val jHero = new JHero(
    "Arya",
    new JHouse("Stark", "Winter is comming")
    ,
    17,
    new JWeapon("Needle", 100),
    null
  )

  println(sHero)
  println(jHero)
}
