package com.agilogy.tutorial.scala

import java.time.LocalTime

object HelloWorld {
  def main(args: Array[String]) {
    val morningGreeting:String = "Good moooorning Maaaangooooooo!"
    val afternoonGreeting = "Good afternoon"
    val nightGreeting = "Good night"
    val now = LocalTime.now
    if(now.isAfter(LocalTime.of(6,0)) && now.isBefore(LocalTime.NOON)){
      println(morningGreeting)
    } else if (now.isAfter(LocalTime.NOON) && now.isBefore(LocalTime.of(21,0))){
      println(afternoonGreeting)
    } else {
      println(nightGreeting)
    }
  }
}
