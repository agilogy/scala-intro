package com.agilogy.tutorial.scala.model

case class Weapon(name: String, damage: Int)
case class House(name: String, words: String)
case class Hero(name: String, house: House, age: Int, mainWeapon: Weapon, inLoveWith: Option[Hero])