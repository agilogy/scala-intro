package com.agilogy.tutorial.java.model;

public class Weapon {
    private String name;
    private int damage;

    public Weapon(String name, int damage) {
        this.name = name;
        this.damage = damage;
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "name='" + name + '\'' +
                ", damage=" + damage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Weapon weapon = (Weapon) o;

        if (damage != weapon.damage) return false;
        return name.equals(weapon.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + damage;
        return result;
    }

    public String getName() {
        return name;
    }

    public int getDamage() {
        return damage;
    }
}
