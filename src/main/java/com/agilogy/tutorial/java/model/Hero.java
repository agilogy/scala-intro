package com.agilogy.tutorial.java.model;

public class Hero {
    private String name;
    private House house;
    private int age;
    private Weapon mainWeapon;
    private Hero inLoveWith;

    public Hero(String name, House house, int age, Weapon mainWeapon, Hero inLoveWith) {
        this.name = name;
        this.house = house;
        this.age = age;
        this.mainWeapon = mainWeapon;
        this.inLoveWith = inLoveWith;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", house=" + house +
                ", age=" + age +
                ", mainWeapon=" + mainWeapon +
                ", inLoveWith=" + inLoveWith +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hero hero = (Hero) o;

        if (age != hero.age) return false;
        if (!name.equals(hero.name)) return false;
        if (!house.equals(hero.house)) return false;
        if (!mainWeapon.equals(hero.mainWeapon)) return false;
        return inLoveWith != null ? inLoveWith.equals(hero.inLoveWith) : hero.inLoveWith == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + house.hashCode();
        result = 31 * result + age;
        result = 31 * result + mainWeapon.hashCode();
        result = 31 * result + (inLoveWith != null ? inLoveWith.hashCode() : 0);
        return result;
    }

    public String getName() {
        return name;
    }

    public House getHouse() {
        return house;
    }

    public int getAge() {
        return age;
    }

    public Weapon getMainWeapon() {
        return mainWeapon;
    }

    public Hero getInLoveWith() {
        return inLoveWith;
    }
}