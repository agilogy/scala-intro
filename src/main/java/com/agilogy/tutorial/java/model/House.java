package com.agilogy.tutorial.java.model;

public class House {
    private String name;
    private String words;

    public House(String name, String words) {
        this.name = name;
        this.words = words;
    }

    @Override
    public String toString() {
        return "House{" +
                "name='" + name + '\'' +
                ", words='" + words + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        House house = (House) o;

        if (!name.equals(house.name)) return false;
        return words.equals(house.words);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + words.hashCode();
        return result;
    }

    public String getName() {
        return name;
    }

    public String getWords() {
        return words;
    }
}
